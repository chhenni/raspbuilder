#!/usr/bin/env bash


#Require root
if [ $(id -u) -ne 0 ]; then
 printf "Script must be run as root, use sudo"
 exit 1
fi


echo "Updating packages"
apt-get update
apt-get upgrade -y
apt-get install x11-xserver-utils unclutter ttf-mscorefonts-installer vim

echo "Installing chromium"
#wget http://ftp.us.debian.org/debian/pool/main/libg/libgcrypt11/libgcrypt11_1.5.0-5+deb7u4_armhf.deb
#wget http://launchpadlibrarian.net/218525709/chromium-browser_45.0.2454.85-0ubuntu0.14.04.1.1097_armhf.deb
#wget http://launchpadlibrarian.net/218525711/chromium-codecs-ffmpeg-extra_45.0.2454.85-0ubuntu0.14.04.1.1097_armhf.deb
sudo dpkg -i libgcrypt11_1.5.0-5+deb7u4_armhf.deb
sudo dpkg -i chromium-codecs-ffmpeg-extra_45.0.2454.85-0ubuntu0.14.04.1.1097_armhf.deb
sudo dpkg -i chromium-browser_45.0.2454.85-0ubuntu0.14.04.1.1097_armhf.deb

echo "Enter password for pi user"
passwd pi &&

CURRENT_HOSTNAME=$(cat /etc/hostname | tr -d " \t\n\r")
NEW_HOSTNAME=$(whiptail --inputbox "Please enter the new hostname (dsctyXXX)" 20 60 "ds" 3>&1 1>&2 2>&3)
echo $NEW_HOSTNAME > /etc/hostname
sudo sed -i "s/127.0.1.1.*$CURRENT_HOSTNAME/127.0.0.1\t$NEW_HOSTNAME/g" /etc/hosts

echo "Enabling SSH server"
update-rc.d ssh enable
invoke-rc.d ssh start


echo "Setting up xwindows and chromium"
cat <<EOF > /etc/xdg/lxsession/LXDE/autostart
@lxpanel --profile LXDE
@pcmanfm --desktop --profile LXDE
#@xscreensaver -no-splash

@xset s off
@xset -dpms
@xset s noblack
 
@set -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium/Default/Preferences
@set -i 's/"exit_type": "Crashed"/"exit_type": "None"/' ~/.config/chromium/Default/Preferences
@chromium-browser --disable-infobars --disable-session-crashed-bubble --noerrdialogs --incognito --kiosk http://info.berg-hansen.no/index.html?device=$NEW_HOSTNAME
EOF

rm /etc/xdg/lxsession/LXDE-pi/autostart
ln -s /etc/xdg/lxsession/LXDE/autostart /etc/xdg/lxsession/LXDE-pi/autostart
rm /home/pi/.config/lxsession/LXDE-pi/autostart

echo "Fixing boot config"
cat <<EOF >> /boot/config.txt
hdmi_force_hotplug=1
hdmi_group=2
hdmi_mode=0x27
EOF

echo "Appending nightly boot to Crontab"
cat <<EOF >> /etc/crontab

18 2    * * *   root    reboot
EOF


echo "Final steps:"
echo "* Run raspi-config to expand root fs"

